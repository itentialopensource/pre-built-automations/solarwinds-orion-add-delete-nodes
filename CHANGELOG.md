
## 0.0.12 [06-21-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/solarwinds-orion-add-delete-nodes!10

---

## 0.0.11 [05-22-2023]

* Merging pre-release/2022.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/solarwinds-orion-add-delete-nodes!9

---

## 0.0.10 [02-14-2022]

* Certified for 2021.2

See merge request itentialopensource/pre-built-automations/solarwinds-orion-add-delete-nodes!8

---

## 0.0.9 [07-02-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/solarwinds-orion-add-delete-nodes!7

---

## 0.0.8 [06-15-2021]

* iap 2021.1 certification

See merge request itentialopensource/pre-built-automations/solarwinds-orion-add-delete-nodes!6

---

## 0.0.7 [06-15-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/solarwinds-orion-add-delete-nodes!5

---

## 0.0.6 [05-27-2021]

* Update AddNode Workflow to remove groups

See merge request itentialopensource/pre-built-automations/staging/solarwinds-orion-add-delete-nodes!3

---

## 0.0.5 [03-23-2021]

* Update AddNode Workflow to remove groups

See merge request itentialopensource/pre-built-automations/staging/solarwinds-orion-add-delete-nodes!3

---

## 0.0.4 [01-12-2021]

* [LB-355] patch/README.md

See merge request itentialopensource/pre-built-automations/staging/solarwinds-orion-add-delete-nodes!2

---

## 0.0.3 [12-22-2020]

* Patch/lb 355

See merge request itentialopensource/pre-built-automations/staging/solarwinds-orion-add-delete-nodes!1

---

## 0.0.2 [12-22-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.6 [10-15-2020]

* updated the template to replace the word Artifact

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!3

---

## 0.0.5 [08-03-2020]

* Fixed repository.url in Package.json

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!2

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
