<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->

<!-- Update the below line with your Pre-Built name -->
# SolarWinds Add/Delete Node

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [Requirements](#requirements)
* [Features](#features)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)


## Overview

This Pre-Built Automation consist of three (3) workflows and two (2) forms that serve as templates for creating and deleting nodes in the SolarWinds Orion server. This Pre-Built was designed and tested on a SolarWinds Orion live server and the [SolarWinds Adapter](https://gitlab.com/itentialopensource/adapters/telemetry-analytics/adapter-solarwinds) was utilized to connect to a live instance.

This Pre-Built consists of the following:

- Operations Manager (SolarWinds-Orion-AddNode)
- Operations Manager (SolarWinds-Orion-DeleteNodes)
- Create WorkFlow (SolarWinds-Orion-AddNode)
- Load Nodes WorkFlow (SolarWinds-Orion-LoadNodes)
- Delete Nodes WorkFlow (SolarWinds-Orion-DeleteNodes)
- JSON Add Node Form (SolarWinds-Orion-AddNode)
- JSON Delete Node Form (SolarWinds-Orion-DeleteNode)
- JST (Filter-NodeData-for-Uri)

_Estimated Run Time_: 1 mins

## Main Workflows

Pictured below are the main workflows for creating and deleting a node.

**Create Node**

<table><tr><td>
<img src="images/CreateNode_Workflow_.png" alt="install" width="600px">
</td></tr></table>  

Below is a payload example required for creating a node.

  ```json
    {
    "IPAddress":"10.16.201.45",
    "EngineID":1,
    "ObjectSubType":"SNMP",
    "SNMPVersion":2,
    "Community":"public",
    "DNS":"googledns.lab.local",
    "SysName":"googledns01.lab.local"
    }
  ```

**Delete Node**

<table><tr><td>
<img src="images/DeleteNodes_Workflow.png" alt="install" width="600px">
</td></tr></table>  

The workflow to delete a node has two parts:

1. The nodes to delete from the Orion server are loaded.

  <table><tr><td>
  <img src="images/LoadNodes_Workflow.png" alt="install" width="600px">
  </td></tr></table>  
  
2. A node list form is loaded by manual task. From this form, select and confirm the nodes for deletion.

  <table><tr><td>
  <img src="images/SelectionNode_Form_for_Deletion_Workflow.png" alt="install" width="600px">
  </td></tr></table>  

## Installation Prerequisites

Users must satisfy the following pre-requisites:

* Itential Automation Platform
  * `^2021.1`

## Requirements

This Pre-Built requires the following:

* [SolarWinds Adapter](https://gitlab.com/itentialopensource/adapters/telemetry-analytics/adapter-solarwinds)

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of Itential Automation Platform (IAP) as listed above in the [Prerequisites](#installation-prerequirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within `App-Admin_Essential`. Simply search for the name of your desired Pre-Built and click the install button.

## How to Run

Use of this Pre-Built assumes you have met the necessary requisites listed in the [Requirements](#requirements) section above.

**Create Node**

1. Complete all required fields for Node Payload on the Create Node input form.

  <table><tr><td>
  <img src="images/CreateNode_Form.png" alt="install" width="600px">
  </td></tr></table> 

2. After running the Create Node automation, login to the SolarWinds Orion Server.
3. Go to Manage Nodes to verify the node is added.
  
  <table><tr><td>
    <img src="images/output.png" alt="install" width="600px">
  </td></tr></table>

**Delete Node**

1. Select the SolarWinds adapter instance on the Delete Node input form.

  <table><tr><td>
  <img src="images/DeleteForm.png" alt="install" width="600px">
  </td></tr></table> 

2. After running the SolarWinds-Orion-DeleteNode automation, verify node deletion in the Manage Nodes console.

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
